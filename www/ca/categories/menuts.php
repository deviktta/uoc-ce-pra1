<?php

require_once __DIR__ . '/../../src/Entity/Game.php';
require_once __DIR__ . '/../../src/Entity/Category.php';
require_once __DIR__ . '/../../src/Service/HtmlService.php';
require_once __DIR__ . '/../../src/Service/SystemService.php';

$current_lang = 'ca';
$category_id = "2";

$category = new Category($category_id);
$title = "Categoria: " . $category->get('name');

?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-category">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12"><?php echo $title ?></h1>
      </div>
      <div class="row featured-list">
        <?php echo Game::renderGameListByCategoryId($current_lang, $category_id); ?>
      </div>
    </main>

  </body>
</html>
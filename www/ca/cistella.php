<?php

require_once __DIR__ . '/../src/Entity/Category.php';
require_once __DIR__ . '/../src/Service/HtmlService.php';
require_once __DIR__ . '/../src/Service/SystemService.php';
require_once __DIR__ . '/../src/Service/CartService.php';

$current_lang = 'ca';
$title = "Cistella";

$post = $_POST;
if (!empty($post['id']) && !empty($post['type'])) {
  // S'ha afegit un product a la cistella.
  CartService::update($post['id'], ($post['type'] === 'add') ? 1 : -1);
}

$cart = new CartService();
$order = $cart->getOrderLines();
?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-cart">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12"><?php echo $title ?></h1>
      </div>
      <?php if (!empty($order)) { ?>
        <div class="row">
        <div class="col-12">
          <table id="cart" style="width: 100%">
            <thead>
              <tr>
                <th>NOM DE L'ARTICLE</th>
                <th>QUANTITAT</th>
                <th>PREU UNITARI</th>
                <th>PREU TOTAL</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($order as $order_line) { ?>
                <tr>
                  <td><?php echo $order_line['name']; ?></td>
                  <td class="quantity">
                    <form method="POST" class="order-line-modification">
                      <input type="hidden" name="id" value="<?php echo $order_line['id']; ?>"/>
                      <input type="hidden" name="type" value="subtract"/>
                      <input type="submit" value="-"/>
                    </form>
                    <span><?php echo $order_line['quantity']; ?></span>
                    <form method="POST" class="order-line-modification">
                      <input type="hidden" name="id" value="<?php echo $order_line['id']; ?>"/>
                      <input type="hidden" name="type" value="add"/>
                      <input type="submit" value="+"/>
                    </form>
                  </td>
                  <td><?php echo $order_line['price_unit_str']; ?> €</td>
                  <td><?php echo $order_line['price_total_str']; ?> €</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          <p class="total-price"><b>TOTAL: <?php echo $cart->printOrderPrice() ?> €</b></p>
          <p class="buy-link"><a class="btn btn-success" href="/ca/comprar.php">COMPRAR</a></p>
        </div>
      </div>
      <?php } else { ?>
        <div class="row">
          <div class="col-12">
            <p>La cistella està buida.</p>
          </div>
        </div>
      <?php } ?>

    </main>

  </body>
</html>
<?php

require_once __DIR__ . '/../../src/Entity/Game.php';
require_once __DIR__ . '/../../src/Entity/Category.php';
require_once __DIR__ . '/../../src/Service/HtmlService.php';
require_once __DIR__ . '/../../src/Service/SystemService.php';
require_once __DIR__ . '/../../src/Service/CartService.php';

$current_lang = 'ca';
$game_id = "5";

$game = new Game($game_id);
$title = "Joc: " . $game->get('name');

$post = $_POST;
if (!empty($post['id']) && !empty($post['quantity'])) {
  // S'ha afegit un product a la cistella.
  CartService::addGameToCart($post['id'], $post['quantity']);
}

?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-game">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row item-content">
        <div class="col-4">
          <img src="/assets/images/joc.jpg" style="width: 100%">
        </div>
        <div class="col-8">
          <h1><?php echo $title ?></h1>
          <p class="item-description"><?php echo $game->get('description'); ?></p>
          <?php echo Game::renderAddGameForm($current_lang, $game_id); ?>
        </div>
      </div>
    </main>

  </body>
</html>
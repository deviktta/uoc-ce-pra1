<?php

require_once __DIR__ . '/../../src/Service/DatabaseService.php';
require_once __DIR__ . '/../../src/Service/SystemService.php';

/**
 * Clase abstracte genèrica per qualsevol tipus d'entitat.
 */
abstract class Entity {
  
  /**
   * Id.
   *
   * @var integer
   */
  protected $id;
  
  /**
   * Constructor d'entitats.
   *
   * @param int $id
   *   Identificador de l'entitat (id de la taula de la base da dades).
   *
   * @return array|null
   *   Les dades de l'entitat en array; NULL si no existeix.
   */
  public function __construct(int $id) {
    try {
      // Obtenir dades de la BBDD.
      $table = static::DB_TABLE;
      $query = "SELECT * FROM $table WHERE id=$id";

      $databaseService = new DatabaseService();
      $db_result = $databaseService->query($query, FALSE);
      $entity = $db_result->fetch_assoc();
      
      // Inicialitzar l'objecte.
      foreach ($entity as $key => $value) {
        $this->{$key} = $value;
      }
    }
    catch (\Error $e) {
      die(__CLASS__ . "::" . __METHOD__ . " ERROR: " . $e->getMessage());
    }
  }
  
  /**
   * Obtenció d'atributs.
   *
   * @param string $attribute
   *   Identificador de l'atribut.
   *
   * @return mixed
   *   Valor de l'atribut, NULL si no existeix.
   */
  public function get(string $attribute) {
    if (!$this->hasAttribute($attribute)) {
      return NULL;
    }
    return $this->{$attribute} ?? NULL;
  }

  /**
   * Desament d'atributs.
   *
   * @param string $attribute
   *   Identificador de l'atribut.
   * @param string $value
   *   Valor de l'atribut.
   *
   * @return Entity
   *   Objecte actual.
   */
  public function set(string $attribute, $value): ApiBase {
    if (!$this->hasAttribute($attribute)) {
      return $this;
    }
    $this->{$attribute} = $value;
    return $this;
  }

  /**
   * Comprovació de l'existencia d'un atribut.
   *
   * @param string|null $attribute
   *   Identificador de l'atribut.
   *
   * @return bool
   *   True/false respecte si existeix o no l'atribut.
   */
  public function hasAttribute(string &$attribute): bool {
    return property_exists(get_class($this), $attribute);
  }
  
  /**
   * Obtenir totes les entitats d'un tipus de la base dades en forma d'array.
   *
   * @return array
   *   Les entitats.
   */
  public static function getAll() {
    $db = new DatabaseService();
    $entities = [];

    $table = static::DB_TABLE;
    $query = "SELECT * FROM $table;";
    $mysql = $db->query($query, FALSE);
    while ($entity = $mysql->fetch_assoc()) {
      $entities[] = $entity;
    }

    return $entities;
  }

}

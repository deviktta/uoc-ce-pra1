<?php

require_once __DIR__ . '/CartService.php';

/**
 * Servei per donar support a tasques pròpies d'una pàgina HTML.
 */
class HtmlService {
  
  public static function getSiteTitle($lang) {
    
    switch ($lang) {

      case 'ca':
        $site_title = "Botiga on-line de jocs de taula";
        break;

      default:
        die(__CLASS__ . "::" . __METHOD__ . " - Language not found");
        break;

    }
    
    return $site_title;
  }

  /**
   * Obtenir el <head> d'una pàgina HTML.
   *
   * @param string $lang
   *   Idioma del contingut.
   *
   * @param string $title
   *   Títol de la pàgina.
   *
   * @return string
   *   Codi HTML del <head>.
   */
  public static function getHead(string $lang, string $title) {
    return ""
    . "<head>\n"
      . "<meta charset=\"UTF-8\">\n"
      . "<link rel=\"stylesheet\" href=\"https://pro.fontawesome.com/releases/v5.10.0/css/all.css\" integrity=\"sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p\" crossorigin=\"anonymous\"/>"
      . "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n"
      . "<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n"
      . "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n"
      . "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n"
      . "<link rel=\"stylesheet\" href=\"/assets/css/styles.css\">\n"
      . "<title>$title | Botiga on-line de jocs de taula</title>\n"
    . "</head>\n";
  }
  

  /**
   * Obtenir el <header> de la nostra pàgina web, on s'inclou el títol
   * i el menú  principal de la pàgina.
   *
   * @param string $lang
   *   Idioma del contingut.
   *
   * @return string
   *   Codi HTML del <header>.
   */
  public static function renderHeader(string $lang) {
    $current_quantity = CartService::getCurrentQuantity();
    switch ($lang) {

      case 'ca':
        $header = ""
          . "<header id=\"site-header\" class=\"container\">"
            . "<div id=\"header-title\" class=\"row\">"
              . "<p class=\"col-12 logo\">BOTIGA ON-LINE DE JOCS DE TAULA</p>"
            . "</div>"
            . "<nav id=\"header-menu\" class=\"row\">"
              . "<ul class=\"col-12\">"
                . "<li><i class=\"fas fa-home\"></i> <a href=\"/$lang/inici.php\">INICI</a></li>"
                . "<li><i class=\"fas fa-shopping-cart\"></i> <a href=\"/ca/cistella.php\">CISTELLA ($current_quantity)</a></li>"
              . "</ul>"
            . "</nav>";

        if (SystemService::isAdmin()) {
          $header .= "<div class=\"row\">"
            . "<p class=\"col-12\" style=\"color: #FF0000; text-align: center; margin-top: 20px;\">Heu iniciat sessió com administrador, per tancar sessió premeu <a href=\"/ca/inici.php?action=logout\" style=\"color: #FF0000;\">aquí</a>.</p>"
            . "</div>";
        }

        $header .= "</header>";
        break;

      default:
        die(__CLASS__ . "::" . __METHOD__ . " - Language not found");
        break;

    }

    return $header;
  }

}